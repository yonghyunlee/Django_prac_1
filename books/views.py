from django.views.generic.base import TemplateView
from django.views.generic import ListView
from django.views.generic import DetailView
from books.models import Book, Author, Publisher
from django.http import HttpResponse
import logging
logger = logging.getLogger(__name__)


#-- TemplateView
class BooksModelView(TemplateView):
    template_name = 'books/index.html'

    def get_context_data(self, **kwargs):
        context = super(BooksModelView, self).get_context_data(**kwargs)
        context['object_list'] = ['Book', 'Author', 'Publisher']
        return context


#-- ListView
class BookList(ListView):
    model = Book

    def get_queryset(self):
        logger.debug("sss")
        last_book = super().get_queryset().latest('publication_date')
        logger.debug(last_book.publication_date.strftime('%a, %d %b %Y %H:%M:%S GMT'))
        return super().get_queryset()


class AuthorList(ListView):
    model = Author


class PublisherList(ListView):
    model = Publisher


#-- DetailView
class BookDetail(DetailView):
    model = Book


class AuthorDetail(DetailView):
    model = Author


class PublisherDetail(DetailView):
    model = Publisher


class BookListView(ListView):
    model = Book

    def head(self, *args, **kwargs):
        last_book = self.get_queryset().latest('publication_date')
        response = HttpResponse('')
        response['Last-Modified'] = last_book.publication_date.strftime('%a, %d %b %Y %H:%M:%S GMT')
        return response
