from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from polls.forms import NameForm, ContactForm
from .models import Choice, Question
from django.views.generic import View, ListView, DetailView
from django.views.generic.edit import FormView
from books.models import Book
import logging
logger = logging.getLogger(__name__)


class BookListView(ListView):
    model = Book

    def head(self, *args, **kwargs):
        last_book = self.get_queryset().latest('publication_date')
        response = HttpResponse('')
        response['Last-Modified'] = last_book.publication_date.strftime('%a, %b %d %Y %H:%M:%S GMT')
        return response


class MyView(View):
    def get(self, request):
        return HttpResponse('result')


def index(request):
    latest_question_list = Question.objects.all().order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})


def vote(request, question_id):
    logger.debug("vote().question_id: %s" % question_id)
    p = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))


def contact(request):
    if request.method == 'POST':
        pass
    else:
        form = ContactForm()
        return render(request, 'polls/contact.html', {'form':form})


def hello(request):
    if request.method == 'POST':
        form = NameForm(request.POST)
        if form.is_valid():
            your_name = form.cleaned_data['your_name']
            return HttpResponseRedirect('/polls/welcome/'+your_name)
    else:
        form = NameForm()
    return render(request, 'polls/hello.html', {'form': form})


def welcome(request, name):
    context = {'name': name}
    return render(request, 'polls/welcome.html', context)


class IndexView(ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(DetailView):
    model = Question
    template_name = 'polls/results.html'