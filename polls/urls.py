from django.conf.urls import url
from . import views
from .views import MyView

app_name = 'polls'


urlpatterns = [
   url(r'^$', views.IndexView.as_view(), name='index'),
   url(r'^hello$', views.hello, name='hello'),
   url(r'^contact$', views.contact, name='contact'),
   url(r'^welcome/(?P<name>\w+)$', views.welcome, name='welcome'),
   url(r'^(?P<pk>\d+)/$', views.DetailView.as_view(), name='detail'),
   url(r'^(?P<question_id>\d+)/vote/$', views.vote, name='vote'),
   url(r'^(?P<pk>\d+)/results/$', views.ResultsView.as_view(), name='results'),
   url(r'^about/', MyView.as_view()),
]
