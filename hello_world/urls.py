from django.urls import path, include
from django.conf.urls import url
from . import views
from .views import MyView

urlpatterns = [
    path('', views.index),
    path('your-name/', views.get_name),
    path('contact/', views.contact),
    url(r'^thanks/(?P<name>\w+)$', views.thanks),
    url(r'^about/', MyView.as_view()),
    url(r'^formView/', views.MyFormView.as_view())

]
