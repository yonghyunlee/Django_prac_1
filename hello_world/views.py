from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View
from django.views.generic.edit import FormView
from .models import ToDoList, NameForm, ContactForm

# Create your views here.


def index(req):
    toDoList = ToDoList.objects.all()
    context = {
        'toDoList': toDoList
    }
    return render(req, 'hello_word/index.html', context)


def get_name(req):
    if req.method == 'POST':
        form = NameForm(req.POST)
        if form.is_valid():
            new_name = form.cleaned_data['your_name']
            print(new_name)
            return HttpResponseRedirect('/thanks/'+new_name)
    else:
        form = NameForm()

    return render(req, 'hello_word/name.html', {'form': form})


def contact(req):
    if req.method == 'POST':
        pass
    else:
        form = ContactForm()

    return render(req, 'hello_word/name.html', {'form': form})


def thanks(req, name):
    context = {'name': name}
    return render(req, 'hello_word/thanks.html', context)


class MyView(View):
    def get(self, requset):
        return HttpResponse('result')


class MyFormView(FormView):
    form_class = NameForm
    template_name = 'hello_word/form_template.html'
    success_url = '/thanks/'

    def form_valid(self, form):
        return super(MyFormView, self).form_valid(form)
