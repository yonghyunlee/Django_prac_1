# Generated by Django 2.0.5 on 2018-05-29 14:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hello_world', '0003_auto_20180529_2345'),
    ]

    operations = [
        migrations.AddField(
            model_name='todolist',
            name='endDate',
            field=models.DateTimeField(null=True),
        ),
    ]
