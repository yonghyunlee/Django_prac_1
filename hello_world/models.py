from django.db import models
from django import forms


# Create your models here.

class ToDoList(models.Model):
    todo = models.CharField(max_length=10)
    introduction = models.TextField(null=False)
    startDate = models.DateTimeField()
    endDate = models.DateTimeField(null=True)

    def __str__(self):
        return self.todo


class NameForm(forms.Form):
    your_name = forms.CharField(max_length=10)


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)
