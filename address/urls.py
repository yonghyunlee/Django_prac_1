from django.conf.urls import url
from . import views

app_name = 'address'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^address/$', views.AddressListView.as_view(), name='address_list'),
    url(r'^address/(?P<pk>\d+)/$', views.AddressDetailView.as_view(), name='address_detail')
]