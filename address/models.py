from django.db import models

# Create your models here.


class Address(models.Model):
    name = models.CharField(max_length=100, default='김땡땡')
    phone = models.CharField(max_length=20)
    email = models.EmailField()
    address = models.CharField(max_length=100, default='한밭대학교')

    def __str__(self):
        return self.name
