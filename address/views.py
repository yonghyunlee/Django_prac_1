from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic import ListView
from django.views.generic import DetailView
from address.models import Address


# Create your views here.
class IndexView(TemplateView):
    template_name = 'address/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = ['Address']
        return context


class AddressListView(ListView):
    model = Address


class AddressDetailView(DetailView):
    model = Address
